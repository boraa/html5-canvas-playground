import { PluginBase } from '../../core/PluginBase';

export class AutoSave extends PluginBase {
	init(): void {
		setInterval(() => {
			this.eventManager.publish('CodeEditor.TriggerSaveCode', { type: 'autosave' });
		}, 5000);
	}
}