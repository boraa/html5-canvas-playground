import { PluginBase } from '../../core/PluginBase';
import FooterComponent from './FooterComponent.jsx';
import { IPanes } from '../../core/IPanes';

export class Footer extends PluginBase {
	init(): void {
		const footerMessage: HTMLDivElement = <HTMLDivElement>document.getElementById('footer-message');
		footerMessage.innerHTML = 'Ready';

		this.eventManager.subscribe('Core.StatusBarMessage', (e: any) => {
			if (e.message) {
				footerMessage.innerHTML = e.message;
			} else {
				footerMessage.innerHTML = 'Ready';
			}        
		});
	}

	registerUIElement(panes: IPanes): void {
		panes.footer.push(FooterComponent);
	}
}