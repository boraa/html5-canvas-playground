import 'codemirror/lib/codemirror.js';
import 'codemirror/mode/javascript/javascript.js';
import 'codemirror/addon/hint/show-hint.js';
import 'codemirror/addon/hint/javascript-hint.js';
import 'codemirror/addon/search/search.js';
import 'codemirror/addon/search/searchcursor.js';
import 'codemirror/addon/search/jump-to-line.js';
import 'codemirror/addon/search/matchesonscrollbar.js';
import 'codemirror/addon/search/match-highlighter.js';
import 'codemirror/addon/fold/foldgutter.js';
import 'codemirror/addon/fold/foldcode.js';
import 'codemirror/addon/fold/comment-fold.js';
import 'codemirror/addon/fold/brace-fold.js';
import 'codemirror/addon/fold/indent-fold.js';
import 'codemirror/addon/dialog/dialog.js';
import 'codemirror/addon/edit/trailingspace.js';
import 'codemirror/addon/scroll/annotatescrollbar.js';
import 'codemirror/addon/selection/active-line.js';
import 'codemirror/addon/selection/mark-selection.js';
import 'codemirror/addon/display/fullscreen.js';
import 'codemirror/addon/display/placeholder.js';
import 'codemirror/addon/lint/lint.js';
import 'codemirror/addon/lint/javascript-lint.js';
import CodeMirror from 'codemirror';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/hint/show-hint.css';
import 'codemirror/theme/material.css';
import 'codemirror/addon/dialog/dialog.css';
import 'codemirror/addon/fold/foldgutter.css';
import 'codemirror/addon/display/fullscreen.css';
import 'codemirror/addon/lint/lint.css';

import { JSHINT } from 'jshint';

import { PluginBase } from '../../core/PluginBase';

import './codeeditor.css';

import Editor from './Editor.jsx';
import { IPanes } from '../../core/IPanes';

(<any>window).JSHINT = JSHINT; 

declare module 'codemirror' { 
	export const hint: Hint;
	interface Hint { javascript: any; }

	interface EditorConfiguration { 
		hint?: any; autoCloseBrackets?: boolean; fullScreen?: boolean; showTrailingSpace?: boolean;
	}

	interface CommandActions { autocomplete: Function; }
}

export class CodeEditor extends PluginBase {
	init(): void {
		const that = this;
		const codeInput: HTMLTextAreaElement = <HTMLTextAreaElement>document.getElementById('code-input');
		const editor: CodeMirror.Editor = CodeMirror.fromTextArea(codeInput, {
			theme: 'material',
			mode: { name: 'javascript', globalVars: true },
			lineNumbers: true,
			gutters: [ 'CodeMirror-lint-markers', 'CodeMirror-linenumbers', 'CodeMirror-foldgutter'  ],
			extraKeys: { 
				'Ctrl-Space': 'autocomplete',
				'Ctrl-S': () => {
					if (!editor.isClean()) {
						that.publishSaveCodeEvent(editor, 'save');
						editor.markClean();
					}
				},
				'Alt-F': 'findPersistent',
				'F11': cm => {
					cm.setOption('fullScreen', !cm.getOption('fullScreen'));
				},
				'Esc': cm => {
					if (cm.getOption('fullScreen')) {
						cm.setOption('fullScreen', false);
					}
				}
			},
			lineWrapping: true,
			autofocus: true,
			hint: CodeMirror.hint.javascript,
			autoCloseBrackets: true,
			smartIndent: true,
			lint: true,
			styleActiveLine: true,
			foldGutter: true,
			showTrailingSpace: true,
			highlightSelectionMatches: { showToken: /\w/, annotateScrollbar: true, delay: 100 }
		});

		editor.on('keydown', (cm: CodeMirror.Editor, event: KeyboardEvent) => {
			if (!(event.ctrlKey) && (event.keyCode >= 65 && event.keyCode <= 90) || 
			(event.keyCode >= 97 && event.keyCode <= 122) || 
			(event.keyCode >= 46 && event.keyCode <= 57)) {
				CodeMirror.commands.autocomplete(cm, null, {completeSingle: false});
			}
		});

		editor.on('change', () => {
			this.updateIsDirty(editor);
		});

		const codeMirrorElement: HTMLDivElement = document.querySelector('div.CodeMirror');
		if (codeMirrorElement) {
			codeMirrorElement.style.fontSize = '18px';
			codeMirrorElement.style.height = (document.getElementById('editor').parentElement.offsetHeight - 35) + 'px';
		}

		const runBtn: HTMLInputElement = <HTMLInputElement>document.getElementById('btn-run');
		runBtn.onclick = () => that.eventManager.publish('CodeEditor.RunCode', { code: editor.getValue() });

		this.eventManager.subscribe('CodeEditor.InitCode', (e: any) => {
			if (e.code) {
				editor.setValue(e.code);
			}
		});

		this.eventManager.subscribe('CodeEditor.TriggerSaveCode', (e: any) => {
			that.publishSaveCodeEvent(editor, e.type);
		});
	}

	registerUIElement(panes: IPanes): void {
		panes.left.push(Editor);
	}

	publishSaveCodeEvent(editor: CodeMirror.Editor, type: string): void {
		const code: string = editor.getValue();
		if (!editor.isClean()) {
			this.eventManager.publish('CodeEditor.SaveCode', { code: code, type: type });

			editor.markClean();
		}

		this.updateIsDirty(editor);
	}

	updateIsDirty(editor: CodeMirror.Editor): void {
		const isDirtySpan: HTMLSpanElement = <HTMLSpanElement>document.querySelector('div#editor .is-dirty');
		isDirtySpan.innerText = editor.isClean() ? '' : '*';
	}
}