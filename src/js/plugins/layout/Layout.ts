import { PluginBase } from '../../core/PluginBase';
import Split from '../../../lib/split.min.js';

import './layout.css';

export class Layout extends PluginBase {
	init(): void {
		const splitInstance = Split([ '.left-pane', '.right-pane' ], { gutterSize: 7 });
		// splitInstance.collapse(0);
	}
}