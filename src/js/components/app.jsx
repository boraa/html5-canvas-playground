import { h, Component } from 'preact';

import PaneContainer from './PaneContainer.jsx';

export default class App extends Component {
	render() {
		const { ...props } = this.props;
		return (
			<PaneContainer left={props.left} right={props.right} footer={props.footer} />
		);
	};
}