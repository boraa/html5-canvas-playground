export class EventManager {
	private registry: { [topic: string]: Function[]; } = {};

	subscribeMany(topics: string[], callback: Function) {
		const len = topics.length;
		for (let i = 0; i < len; i++) {
			const topic = topics[i];
			this.subscribe(topic, callback);
		}
	}

	subscribe(topic: string, callback: Function) {
		const registryItem = this.registry[topic];
		if (!registryItem) {
			this.registry[topic] = [callback];
		} else {
			registryItem.push(callback);
		}
	}

	publish(topic: string, item: any) {
		const registryItem = this.registry[topic];
		if (registryItem) {
			const len = registryItem.length;
			for (let i = 0; i < len; i++) {
				const callback = registryItem[i];
				callback(item);
			}
		}
	}

	reset() {
		this.registry = {};
	}
}