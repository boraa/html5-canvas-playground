import { EventManager } from './EventManager';
import { IPlugin } from './IPlugin';
import { IPanes } from './IPanes';

export class Core {
	private _eventManager: EventManager;
	private _plugins: IPlugin[];
	private _panes: IPanes;
	
	public get panes(): IPanes {
		return this._panes;
	}

	constructor() {
		this._eventManager = new EventManager();
		this._plugins = [];
		this._panes = { left: [], right: [], footer: [] };
	}

	registerPlugin(plugin: IPlugin) {
		plugin.setEventManager(this._eventManager);
		plugin.registerUIElement(this._panes);
		this._plugins.push(plugin);
	}

	registerPlugins(plugins: IPlugin[]) {
		plugins.forEach(plugin => {
			this.registerPlugin(plugin);
		});
	}

	init(): void {
		this._plugins.forEach(plugin => {
			plugin.start();
		});
		this._eventManager.publish('Core.Init', {});
	}
}