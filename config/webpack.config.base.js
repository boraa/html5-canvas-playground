const helpers = require('./helpers'),
      CopyWebpackPlugin = require('copy-webpack-plugin');

let config = {
  entry: {
    'main': helpers.root('/src/js/Main.ts')
  },
  output: {
    path: helpers.root('./dist'),
    filename: 'js/[name].[hash].js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.ts', '.js', '.html']
  },
  resolveLoader: {
    modules: [
      'node_modules'
    ]
  },
  module: {
    rules: [{
      test: /\.ts$/,
      exclude: /node_modules/,
      enforce: 'pre',
      loader: 'tslint-loader'
    },
    {
      test: /\.ts$/,
      loader: 'ts-loader'
    },
    {
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env'],
        plugins: [
          ['@babel/transform-react-jsx', { pragma: 'h' }]
        ]
      }
    },
    {
      test: /\.html$/,
      loader: 'raw-loader'
    }
    ],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        { from: 'src/js/plugins/canvaspreview/canvasplayground.js', to: './js/plugins/canvaspreview/canvasplayground.js' },
        { from: 'src/img', to: './img' }
      ]
    })
  ]
};

module.exports = config;
